package foo

import "testing"

func TestFoo(t *testing.T) {
	if Show() != "foo" {
		t.Errorf("expected foo got other thing")
	}
}
