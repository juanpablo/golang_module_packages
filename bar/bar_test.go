package bar

import (
	"testing"
)

func TestBar(t *testing.T) {
	if Show() != "bar" {
		t.Errorf("expected bar got other thing")
	}
}
